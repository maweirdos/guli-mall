package com.maweirdos.gulimall.search.controller;

import com.maweirdos.gulimall.common.es.SkuEsModel;
import com.maweirdos.gulimall.common.exception.BizCodeEnum;
import com.maweirdos.gulimall.common.utils.R;
import com.maweirdos.gulimall.search.service.ProductSaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @Author: Maweirdos
 * @Date: 2024/3/27 20:24
 */

@RequestMapping("/search/save")
@RestController
public class ElasticSaveController {
    @Autowired
    private ProductSaveService productSaveService;

    @RequestMapping("product/up")
    public R productUp(@RequestBody List<SkuEsModel> skuEsModels) throws IOException {


        boolean status=false;
        try {
            status = productSaveService.productStatusUp(skuEsModels);
        } catch (IOException e) {
            //log.error("商品上架错误{}",e);

            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }

        if(status){
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(),BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }else {
            return R.ok();
        }
    }
}
