package com.maweirdos.gulimall.search.service;

import com.maweirdos.gulimall.search.vo.SearchParam;
import com.maweirdos.gulimall.search.vo.SearchResult;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/4 20:24
 */

public interface MallSearchService {

    /**
     * @param param 检索的所有参数
     * @return  返回检索的结果，里面包含页面需要的所有信息
     */
    SearchResult search(SearchParam param);
}
