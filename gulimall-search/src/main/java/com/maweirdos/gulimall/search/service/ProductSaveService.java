package com.maweirdos.gulimall.search.service;

import com.maweirdos.gulimall.common.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @Author: Maweirdos
 * @Date: 2024/3/27 20:40
 */
public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
