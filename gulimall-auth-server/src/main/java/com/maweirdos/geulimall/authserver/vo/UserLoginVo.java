package com.maweirdos.geulimall.authserver.vo;

import lombok.Data;
@Data
public class UserLoginVo {

    private String loginacct;

    private String password;
}
