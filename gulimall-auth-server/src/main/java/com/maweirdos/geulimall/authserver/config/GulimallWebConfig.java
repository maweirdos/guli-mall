package com.maweirdos.geulimall.authserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/12 22:01
 */
@Configuration
public class GulimallWebConfig implements WebMvcConfigurer {

    /**
     * 地址直接返回页面，没有其他逻辑，使用视图映射
     * @param registry
     */
    @Override
    public void addViewControllers(org.springframework.web.servlet.config.annotation.ViewControllerRegistry registry) {
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/reg.html").setViewName("reg");
    }
}
