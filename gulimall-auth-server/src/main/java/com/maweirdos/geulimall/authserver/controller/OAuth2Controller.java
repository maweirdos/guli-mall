package com.maweirdos.geulimall.authserver.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.maweirdos.geulimall.authserver.feign.MemberFeignService;
import com.maweirdos.geulimall.authserver.vo.SocialUser;
import com.maweirdos.gulimall.common.utils.R;
import com.maweirdos.gulimall.common.vo.MemberResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.maweirdos.gulimall.common.constant.AuthServerConstant.LOGIN_USER;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/18 22:01
 */
@Slf4j
@Controller
public class OAuth2Controller {

    public static final String ClientId = "7f3b190a546a31337f72262954ef8950975d2dfede07e1d96d6158cafb538c9b";
    public static final String ClientSecret = "9ebdb65c35aac782ef6cfd105bdec098aa452cde97574a19b8279ea341d3f653";
    public static final String RedirectUrl = "http://auth.gulimall.com/oauth/gitee/success";

    @Autowired
    private MemberFeignService memberFeignService;

    @GetMapping("/oauth/gitee/success")
    public String Gitee(@RequestParam("code") String code, HttpSession session) throws Exception {
        // 根据 code 来获取access token
        String url = "https://gitee.com/oauth/token?grant_type=authorization_code" +
                "&client_id=" + ClientId +
                "&client_secret=" + ClientSecret +
                "&code=" + code +
                "&redirect_uri=" + RedirectUrl;
        // 使用HttpClient 发送post请求
        HttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        HttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() == 200) {
            //获取到了access_token,转为通用社交登录对象
            String json = EntityUtils.toString(response.getEntity(), "UTF-8");
            SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
            //知道了哪个社交用户
            //1）、当前用户如果是第一次进网站，自动注册进来（为当前社交用户生成一个会员信息，以后这个社交账号就对应指定的会员）
            //登录或者注册这个社交用户
            System.out.println(socialUser.getAccess_token());
            //调用远程服务
            R oauthLogin = memberFeignService.oauthLogin(socialUser);
            if (oauthLogin.getCode() == 0) {
                MemberResponseVo data = oauthLogin.getData("data", new TypeReference<MemberResponseVo>() {});
                log.info("登录成功：用户信息：{}",data.toString());

                //1、第一次使用session，命令浏览器保存卡号，JSESSIONID这个cookie
                //以后浏览器访问哪个网站就会带上这个网站的cookie
                //TODO 1、默认发的令牌。当前域（解决子域session共享问题）
                //TODO 2、使用JSON的序列化方式来序列化对象到Redis中
                session.setAttribute(LOGIN_USER,data);

                //2、登录成功跳回首页
                return "redirect:http://gulimall.com";
            } else {

                return "redirect:http://auth.gulimall.com/login.html";
            }
        }else {
            return "redirect:http://auth.gulimall.com/login.html";
        }
    }
}
