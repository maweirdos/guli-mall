package com.maweirdos.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @Author: Maweirdos
 * @Date: 2024/3/3 20:48
 */
@Configuration
public class CorsConfig {

    /**
     * 跨域配置
     * ctrl+f12 查看方法列表
     * ctrl+h 查看类的继承关系
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //1.配置跨域
        corsConfiguration.addAllowedOrigin("*");
        //2.配置是否发送cookie信息
        corsConfiguration.setAllowCredentials(true);
        //3.配置允许的请求方式
        corsConfiguration.addAllowedMethod("*");
        //4.配置允许的头信息
        corsConfiguration.addAllowedHeader("*");
        //5.配置有效时长
        corsConfiguration.setMaxAge(3600L);
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }
}
