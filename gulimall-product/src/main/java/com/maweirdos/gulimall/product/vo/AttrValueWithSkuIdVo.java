package com.maweirdos.gulimall.product.vo;

import lombok.Data;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/8 21:47
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
