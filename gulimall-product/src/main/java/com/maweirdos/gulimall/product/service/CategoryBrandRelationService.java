package com.maweirdos.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.product.entity.BrandEntity;
import com.maweirdos.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-28 22:57:11
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void savedetail(CategoryBrandRelationEntity categoryBrandRelation);

    void updateByBrandId(Long brandId, String name);

    void updateByCategoryId(Long catId, String name);

    List<BrandEntity> getBrandsByCatId(Long catId);
}

