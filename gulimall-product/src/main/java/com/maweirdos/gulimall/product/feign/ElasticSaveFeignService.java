package com.maweirdos.gulimall.product.feign;

import com.maweirdos.gulimall.common.es.SkuEsModel;
import com.maweirdos.gulimall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Author: Maweirdos
 * @Date: 2024/3/27 21:06
 */

@FeignClient("gulimall-search")
public interface ElasticSaveFeignService {

    @RequestMapping("/search/save/product/up")
    R productUp(@RequestBody List<SkuEsModel> skuEsModels);
}
