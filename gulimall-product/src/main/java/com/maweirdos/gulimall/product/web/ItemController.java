package com.maweirdos.gulimall.product.web;

import com.maweirdos.gulimall.product.service.SkuInfoService;
import com.maweirdos.gulimall.product.vo.SkuItemVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * @Author: Maweirdos
 * @Date: 2024/3/30 14:45
 */
@Controller
public class ItemController {
    @Resource
    private SkuInfoService skuInfoService;

    @GetMapping({"/{skuId}.html"})
    public String skuItem(@PathVariable("skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {
        SkuItemVo vos = skuInfoService.item(skuId);
        model.addAttribute("item",vos);
        return "item";
    }

}
