package com.maweirdos.gulimall.product.dao;

import com.maweirdos.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-28 22:57:11
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
