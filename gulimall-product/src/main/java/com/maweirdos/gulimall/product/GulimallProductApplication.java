package com.maweirdos.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

// 开启远程调用，扫描指定包下的FeignClient
@EnableFeignClients(basePackages = "com.maweirdos.gulimall.product.feign")
@EnableDiscoveryClient
@MapperScan("com.maweirdos.gulimall.product.dao")
@EnableRedisHttpSession
@SpringBootApplication
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
