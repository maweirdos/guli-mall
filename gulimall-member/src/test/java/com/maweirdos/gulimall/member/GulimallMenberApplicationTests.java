package com.maweirdos.gulimall.member;

import com.maweirdos.gulimall.member.entity.MemberEntity;
import com.maweirdos.gulimall.member.service.MemberService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallMenberApplicationTests {

    @Autowired
    MemberService memberService;

    @Test
    public void contextLoads() {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setUsername("余大嘴");
        memberEntity.setMobile("123456789");
        memberEntity.setCity("上海");
        memberService.save(memberEntity);
    }

}
