package com.maweirdos.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.member.entity.MemberStatisticsInfoEntity;

import java.util.Map;

/**
 * 会员统计信息
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:30:28
 */
public interface MemberStatisticsInfoService extends IService<MemberStatisticsInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

