package com.maweirdos.gulimall.member.Feign;

import com.maweirdos.gulimall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: Maweirdos
 * @Date: 2024/2/29 23:43
 * @Description:设置远程调用的服务名gulimall-coupon，表示此为远程调用方法，这是nacos注册中心主要作用
 */

@Service
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    /**
     * 当member中请求该方法时，会自动转发到gulimall-coupon服务中的/coupon/coupon/member/list路径，
     * 发起对gulimall-coupon服务中memberCoupons()方法的调用
     * @return
     */
    @RequestMapping("/coupon/coupon/member/list")
    public R memberCoupons();
}
