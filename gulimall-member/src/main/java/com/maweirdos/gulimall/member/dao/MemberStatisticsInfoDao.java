package com.maweirdos.gulimall.member.dao;

import com.maweirdos.gulimall.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:30:28
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
