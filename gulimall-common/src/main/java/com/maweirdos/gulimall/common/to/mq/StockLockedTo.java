package com.maweirdos.gulimall.common.to.mq;

import lombok.Data;

/**
 * @Author: Maweirdos
 * @Date: 2024/5/22 22:26
 */
@Data
public class StockLockedTo {

    /** 库存工作单的id **/
    private Long id;

    /** 工作单详情的所有信息 **/
    private StockDetailTo detailTo;
}