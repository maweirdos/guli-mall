package com.maweirdos.gulimall.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: Maweirdos
 * @Date: 2024/5/16 20:54
 */

public class NoStockException extends RuntimeException {

    @Getter
    @Setter
    private Long skuId;

    public NoStockException(Long skuId) {
        super("商品id：" + skuId + "库存不足！");
    }

    public NoStockException(String msg) {
        super(msg);
    }
}
