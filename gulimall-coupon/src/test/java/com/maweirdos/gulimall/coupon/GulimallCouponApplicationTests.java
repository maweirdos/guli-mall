package com.maweirdos.gulimall.coupon;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maweirdos.gulimall.coupon.entity.CouponEntity;
import com.maweirdos.gulimall.coupon.service.CouponService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallCouponApplicationTests {
    @Autowired
    CouponService couponService;

    @Test
    public void contextLoads() {
        //更新操作
        /*BrandEntity brandEntity = new BrandEntity();
        brandEntity.setBrandId(1L);
        brandEntity.setName("华为");
        brandEntity.setDescript("遥遥领先");
        brandService.updateById(brandEntity);
        System.out.println("更新成功");*/

        //新增操作
        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("满100减10");
        couponService.save(couponEntity);
        System.out.println("保存成功");
        //查询操作
    }

}
