package com.maweirdos.gulimall.coupon.dao;

import com.maweirdos.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 20:59:20
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
