package com.maweirdos.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 20:59:20
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

