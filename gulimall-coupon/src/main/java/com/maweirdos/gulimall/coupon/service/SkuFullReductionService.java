package com.maweirdos.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.to.SkuReductionTo;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 20:59:20
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo reductionTo);
}

