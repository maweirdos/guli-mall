package com.maweirdos.gulimall.ware.dao;

import com.maweirdos.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:55:15
 */
@Mapper
public interface WareOrderTaskDetailDao extends BaseMapper<WareOrderTaskDetailEntity> {
	
}
