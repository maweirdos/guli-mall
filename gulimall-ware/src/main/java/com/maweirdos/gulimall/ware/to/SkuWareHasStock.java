package com.maweirdos.gulimall.ware.to;

import lombok.Data;

import java.util.List;

/**
 * @Author: Maweirdos
 * @Date: 2024/5/19 15:52
 */
@Data
public class SkuWareHasStock {
    private Long skuId;
    private Integer num;
    private List<Long> wareId;
}
