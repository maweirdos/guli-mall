package com.maweirdos.gulimall.ware.dao;

import com.maweirdos.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:55:15
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
