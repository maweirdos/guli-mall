package com.maweirdos.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.ware.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:55:15
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

