package com.maweirdos.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:55:15
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);

    WareOrderTaskEntity getOrderTaskByOrderSn(String orderSn);
}

