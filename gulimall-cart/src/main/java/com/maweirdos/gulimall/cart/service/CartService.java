package com.maweirdos.gulimall.cart.service;

import com.maweirdos.gulimall.cart.vo.CartItemVo;
import com.maweirdos.gulimall.cart.vo.CartVo;

import java.util.List;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/29 21:05
 */
public interface CartService {
    CartItemVo addToCart(Long skuId, Integer num);

    /**
     * 获取购物车某个购物项
     * @param skuId
     * @return
     */
    CartItemVo getCartItem(Long skuId);

    CartVo getCart();

    void clearCartInfo(String cartKey);

    void checkItem(Long skuId, Integer checked);

    void changeItemCount(Long skuId, Integer num);

    void deleteIdCartInfo(Integer skuId);

    List<CartItemVo> getUserCartItems();
}
