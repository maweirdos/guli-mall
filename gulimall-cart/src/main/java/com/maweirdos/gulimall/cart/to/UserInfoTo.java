package com.maweirdos.gulimall.cart.to;

import lombok.Data;
import lombok.ToString;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/28 21:20
 */
@Data
@ToString
public class UserInfoTo {
    private Long userId;

    private String userKey;

    /**
     * 是否临时用户
     */
    private Boolean tempUser = false;
}
