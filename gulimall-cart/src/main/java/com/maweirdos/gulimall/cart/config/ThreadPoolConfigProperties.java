package com.maweirdos.gulimall.cart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: Maweirdos
 * @Date: 2024/4/10 21:40
 */
@Data
@ConfigurationProperties(prefix = "gulimall.thread")
@Component
public class ThreadPoolConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
