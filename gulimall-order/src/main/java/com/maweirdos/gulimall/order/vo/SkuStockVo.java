package com.maweirdos.gulimall.order.vo;

import lombok.Data;


@Data
public class SkuStockVo {


    private Long skuId;

    private Boolean hasStock;

}
