package com.maweirdos.gulimall.order.controller;

import com.maweirdos.gulimall.order.entity.OrderReturnReasonEntity;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

/**
 * @Author: Maweirdos
 * @Date: 2024/5/5 21:04
 */
@Controller
public class RabbitController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/sendMessage")
    public String sendMessage(@RequestParam(value = "num",defaultValue = "10") Integer num) {
        for (int i = 0; i < num; i++) {
            OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
            orderReturnReasonEntity.setId(1L+i);
            orderReturnReasonEntity.setName("测试退货原因-"+i);
            rabbitTemplate.convertAndSend("hello-java-exchange"
                    , "hello-java"
                    , orderReturnReasonEntity
                    , new CorrelationData(UUID.randomUUID().toString()));
        }

        return "ok";
    }

}
