package com.maweirdos.gulimall.order.service.impl;

import com.maweirdos.gulimall.order.entity.OrderReturnReasonEntity;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.common.utils.Query;

import com.maweirdos.gulimall.order.dao.OrderItemDao;
import com.maweirdos.gulimall.order.entity.OrderItemEntity;
import com.maweirdos.gulimall.order.service.OrderItemService;


@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 监听消息队列 注解中为数组，可监听多个队列
     * @param message
     * @param orderReturnReasonEntity
     * @param channel
     */
    /*@RabbitListener(queues = {"hello-java-queue"})
    public void receiveMessage(Message message, OrderReturnReasonEntity orderReturnReasonEntity, Channel channel) {
        // 获取消息的body
        byte[] body = message.getBody();
        System.out.println("body"+new String(body));
        // 获取消息的属性
        MessageProperties messageProperties = message.getMessageProperties();
        // 获取消息的头
        Map<String, Object> headers = messageProperties.getHeaders();
        System.out.println("headers"+headers);
        System.out.println("receive message: " + message+"实体内容====>"+orderReturnReasonEntity);
    }*/

}