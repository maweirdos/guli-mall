package com.maweirdos.gulimall.order.dao;

import com.maweirdos.gulimall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:50:20
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
