package com.maweirdos.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.maweirdos.gulimall.common.utils.PageUtils;
import com.maweirdos.gulimall.order.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author maweirdos
 * @email wm1786487276@gmail.com
 * @date 2024-02-29 21:50:20
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

