package com.maweirdos.gulimall.order.config;

import com.maweirdos.gulimall.order.entity.OrderEntity;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 自定义RabbitMq配置类,将队列、交换机、绑定关系交给Spring容器管理
 * @Author: Maweirdos
 * @Date: 2024/5/20 21:46
 */

@Configuration
public class MyRabbitMqConfig {

    /**
     * 监听指定的队列，获取消息
     * @param orderEntity
     * @param channel
     * @param message
     * @throws Exception
     */
    /*@RabbitListener(queues = "order.release.queue")
    public void listener(OrderEntity orderEntity, Channel channel, Message message) throws Exception {
        System.out.println("收到过期的消息，准备关闭订单：" + orderEntity.getOrderSn());
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }*/

    // 1. 定义消息交换机order-event-exchange
    @Bean
    public Exchange orderEventExchange(){
        /*
          交换机构造参数String name, boolean durable, boolean autoDelete, Map<String, Object> arguments
         */
        return new TopicExchange("order-event-exchange", true, false);
    }

    // 2.1 定义死信队列order.delay.queue
    @Bean
    public Queue orderDelayQueue(){
        /*
          队列的全参构造String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
          x-dead-letter-exchange: order-event-exchange
          x-dead-letter-routing-key: order.release.order
          x-message-ttl: 60000
         */
        Map<String,Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange","order-event-exchange");
        arguments.put("x-dead-letter-routing-key","order.release.order");
        arguments.put("x-message-ttl",60000);
        return new Queue("order.delay.queue", true, false, false,arguments);
    }

    // 2.2 定义普通队列order.release.queue
    @Bean
    public Queue orderReleaseQueue(){
        return new Queue("order.release.queue", true, false, false);
    }

    // 3.1 定义绑定关系order.create.order
    @Bean
    public Binding orderCreateBinding(){
        // Binding的全参构造String destination, DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        return new Binding("order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
                null);
    }

    // 3.2 定义绑定关系order.release.order
    @Bean
    public Binding orderReleaseBinding(){
        return new Binding("order.release.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);
    }


    /**
     * 订单释放直接和库存释放进行绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOtherBinding() {

        return new Binding("stock.release.stock.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);
    }
}
