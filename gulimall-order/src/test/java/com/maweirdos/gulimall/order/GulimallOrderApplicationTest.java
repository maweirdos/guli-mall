package com.maweirdos.gulimall.order;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.maweirdos.gulimall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallOrderApplicationTest {

    @Autowired
    private AmqpAdmin amqpAdmin;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 创建一个交换机
     */
    @Test
    public void ceateExchange() {
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("【{}】 created successfully","hello-java-exchange");
    }

    /**
     * 创建一个队列
     */
    @Test
    public void createQueue() {
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("【{}】 created successfully","hello-java-queue");
    }

    /**
     * 创建一个绑定
     */
    @Test
    public void createBinding() {
        Binding binding = new Binding("hello-java-queue", Binding.DestinationType.QUEUE, "hello-java-exchange", "hello-java", null);
        amqpAdmin.declareBinding(binding);
        log.info("【{}】 created successfully","hello-java-binding");
    }

    /**
     * 发送消息
     */
    @Test
    public void sendMessage() {
        OrderReturnReasonEntity orderReturnReasonEntity = new OrderReturnReasonEntity();
        orderReturnReasonEntity.setId(1L);
        orderReturnReasonEntity.setName("测试退货原因2");
        rabbitTemplate.convertAndSend("hello-java-exchange", "hello-java", orderReturnReasonEntity);
    }

}
